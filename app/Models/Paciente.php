<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'apellido',
        'edad',
        'sexo',
        'telefono',
        'direccion',
        'email',
        'dni',
        'tipo_sangre'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'

    ];
}
